//-------------------------------------------------------------------------------------------------
/**
 * @file gnss.h
 *
 * Tool to debug/monitor GNSS device.
 *
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved
 * SPDX-License-Identifier: MPL-2.0
 */
//-------------------------------------------------------------------------------------------------

#include "legato.h"
#include "interfaces.h"

#define MAX_NUMBER_OF_INPUT     10

#define MAX_LEN_OF_EACH_INPUT     28

void GnssMainFunction();

